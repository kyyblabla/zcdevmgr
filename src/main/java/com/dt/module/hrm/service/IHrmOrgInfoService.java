package com.dt.module.hrm.service;

import com.dt.module.hrm.entity.HrmOrgInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-04-13
 */
public interface IHrmOrgInfoService extends IService<HrmOrgInfo> {

}
