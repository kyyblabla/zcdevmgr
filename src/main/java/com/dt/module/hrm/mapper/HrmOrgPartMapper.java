package com.dt.module.hrm.mapper;

import com.dt.module.hrm.entity.HrmOrgPart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-04-13
 */
public interface HrmOrgPartMapper extends BaseMapper<HrmOrgPart> {

}
