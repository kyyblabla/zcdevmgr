package com.dt.module.hrm.mapper;

import com.dt.module.hrm.entity.HrmOrgInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-04-13
 */
public interface HrmOrgInfoMapper extends BaseMapper<HrmOrgInfo> {

}
