package com.dt.module.base.service;

import com.dt.module.base.entity.SysFeedback;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-05-16
 */
public interface ISysFeedbackService extends IService<SysFeedback> {

}
