package com.dt.module.base.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.module.base.entity.SysSession;
import com.dt.module.base.mapper.SysSessionMapper;
import com.dt.module.base.service.ISysSessionService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2018-07-29
 */
@Service
public class SysSessionServiceImpl extends ServiceImpl<SysSessionMapper, SysSession> implements ISysSessionService {

}
