package com.dt.module.base.mapper;

import com.dt.module.base.entity.SysFiles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-04-11
 */
public interface SysFilesMapper extends BaseMapper<SysFiles> {

}
