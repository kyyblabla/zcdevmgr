package com.dt.module.cmdb.service;

import com.dt.module.cmdb.entity.ResHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-04-16
 */
public interface IResHistoryService extends IService<ResHistory> {

}
