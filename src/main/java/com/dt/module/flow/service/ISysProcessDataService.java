package com.dt.module.flow.service;

import com.dt.module.flow.entity.SysProcessData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-06-27
 */
public interface ISysProcessDataService extends IService<SysProcessData> {

}
