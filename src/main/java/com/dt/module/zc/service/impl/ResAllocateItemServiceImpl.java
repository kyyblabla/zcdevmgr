package com.dt.module.zc.service.impl;

import com.dt.module.zc.entity.ResAllocateItem;
import com.dt.module.zc.mapper.ResAllocateItemMapper;
import com.dt.module.zc.service.IResAllocateItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2020-04-25
 */
@Service
public class ResAllocateItemServiceImpl extends ServiceImpl<ResAllocateItemMapper, ResAllocateItem> implements IResAllocateItemService {

}
