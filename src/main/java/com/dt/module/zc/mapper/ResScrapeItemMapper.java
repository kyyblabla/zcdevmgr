package com.dt.module.zc.mapper;

import com.dt.module.zc.entity.ResScrapeItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-05-28
 */
public interface ResScrapeItemMapper extends BaseMapper<ResScrapeItem> {

}
