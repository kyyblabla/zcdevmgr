package com.dt.module.ct.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.module.ct.entity.CtContent;
import com.dt.module.ct.mapper.CtContentMapper;
import com.dt.module.ct.service.ICtContentService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2018-07-30
 */
@Service
public class CtContentServiceImpl extends ServiceImpl<CtContentMapper, CtContent> implements ICtContentService {

}
